create database mariyadayana_week9_hcl;
use mariyadayana_week9_hcl;


create table books(id int,name varchar(100),generes varchar(100),url varchar(10000));
 insert into books values(1,'Good Karama','Autobiography',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVSrQ1kqUbDbd0u4FdMSc9vmxQfQ8EWgGWXg&usqp=CAU");
insert into books values(2,'Don Quixote','Action',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMNiuKE6Hj84ZgS3ONI2kZ-u9isRFdR7mstQ&usqp=CAU");
insert into books values(3,'Robin Sharama','Action',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQruVsbhPmxZmtK6mkTZBJZr233e3aJ5gAQvQ&usqp=CAU");
 insert into books values(4,'Patrick Rothfuss','Action',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTFk9OPmodiBk0r3cWuoHUpjc850Dxum1ot3g&usqp=CAU");
insert into books values(5,'The Unwilling','Action',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRj9hpPYhGtbo5txu9K5msilVpTZ3W8udim0g&usqp=CAU");
 create table users(uid int,username varchar(100),password varchar(100));
 insert into users values(1,'Raj','raj');
 insert into users values(2,'Ram','ram');
 create table like_books(id int,name varchar(100),generes varchar(100),url varchar(10000));
insert into like_books values(1,'Good Karama','Autobiography',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVSrQ1kqUbDbd0u4FdMSc9vmxQfQ8EWgGWXg&usqp=CAU");
insert into like_books values(2,'Don Quixote','Action',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMNiuKE6Hj84ZgS3ONI2kZ-u9isRFdR7mstQ&usqp=CAU");
insert into like_books values(3,'Robin Sharama','Action',"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQruVsbhPmxZmtK6mkTZBJZr233e3aJ5gAQvQ&usqp=CAU");
create table admin(adminname varchar(100),password varchar(100));