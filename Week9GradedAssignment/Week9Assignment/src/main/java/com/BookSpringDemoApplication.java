package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication(scanBasePackages = "com")
@EntityScan(basePackages = "com.hcl.greatlearning.week9assign.bean")
@EnableJpaRepositories(basePackages = "com.hcl.greatlearning.week9assign.dao")
public class BookSpringDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookSpringDemoApplication.class, args);
		System.err.println("Server running on port number 8080");
	}

}
