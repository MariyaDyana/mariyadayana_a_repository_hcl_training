package com.hcl.greatlearning.week9assign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.greatlearning.week9assign.bean.Admin;
import com.hcl.greatlearning.week9assign.service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminService adminService;
	
	@PostMapping(value="register",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String register(@RequestBody Admin admin) {
		return adminService.register(admin);
	}
	@PostMapping(value="login",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String loginAdmin(@RequestBody Admin admin) {
		return adminService.loginAdmin(admin);
		}
	@GetMapping(value="logout")
	public String logout() {
		return"logout successfully";
	}
}
