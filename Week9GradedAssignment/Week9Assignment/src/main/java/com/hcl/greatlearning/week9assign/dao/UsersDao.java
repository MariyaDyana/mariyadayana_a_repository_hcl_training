package com.hcl.greatlearning.week9assign.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.week9assign.bean.Users;

@Repository
public interface UsersDao extends JpaRepository<Users, Integer> {

}
