package com.hcl.greatlearning.week9assign.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.week9assign.bean.Books;

@Repository
public interface BooksDao  extends JpaRepository<Books, Integer> {

}
