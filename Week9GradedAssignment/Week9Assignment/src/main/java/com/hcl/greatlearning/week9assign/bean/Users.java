package com.hcl.greatlearning.week9assign.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter					 
@Getter					 
@ToString				 
@NoArgsConstructor			
@RequiredArgsConstructor
public class Users {
@Id
private int uid;
private String username;
private String password;

public Users() {
	super();
	// TODO Auto-generated constructor stub
}


public Users(int uid, String username, String password) {
	super();
	this.uid = uid;
	this.username = username;
	this.password = password;
}



public int getUid() {
	return uid;
}


public void setUid(int uid) {
	this.uid = uid;
}


public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}


}
