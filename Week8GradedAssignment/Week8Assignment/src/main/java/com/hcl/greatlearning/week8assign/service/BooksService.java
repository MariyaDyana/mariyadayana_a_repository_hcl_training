package com.hcl.greatlearning.week8assign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.week8assign.bean.Books;
import com.hcl.greatlearning.week8assign.bean.Users;
import com.hcl.greatlearning.week8assign.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	
	public List<Books> getAllBook() { 
		return booksDao.getAllBook();
	}
    
	public List<Users>getAllUser(){
		return booksDao.getAllUser();
		
	}
}
