package com.hcl.greatlearning.week8assign.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.greatlearning.week8assign.bean.Books;
import com.hcl.greatlearning.week8assign.service.BooksService;

@Controller
public class BooksController {
	@Autowired
	BooksService booksService;

	@GetMapping(value = "display")
	public ModelAndView getAllBook(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		List<Books> listOfBook = booksService.getAllBook();
		req.setAttribute("products", listOfBook);
		req.setAttribute("msg", " Book Details");
		mav.setViewName("display.jsp");
		return mav;
	}
	
	@GetMapping(value = "like")
	public ModelAndView likeBooks(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(req.getParameter("id"));
		System.out.println("Book id is "+id);
		List<Books> listOfBook = booksService.getAllBook();
		req.setAttribute("products", listOfBook);
		req.setAttribute("msg", " Book Details");
		mav.setViewName("like.jsp");
		return mav;
	}


}
