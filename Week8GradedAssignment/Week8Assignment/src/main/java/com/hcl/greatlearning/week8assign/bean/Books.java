package com.hcl.greatlearning.week8assign.bean;

public class Books {
private int id;
private String name;
private String generes;
private String url;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGeneres() {
	return generes;
}
public void setGeneres(String generes) {
	this.generes = generes;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}

}
