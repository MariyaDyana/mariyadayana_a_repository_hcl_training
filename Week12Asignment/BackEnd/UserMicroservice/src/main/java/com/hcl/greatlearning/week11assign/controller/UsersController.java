package com.hcl.greatlearning.week11assign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.greatlearning.week11assign.bean.Users;
import com.hcl.greatlearning.week11assign.service.UsersService;



@RestController
@RequestMapping("/users")
@CrossOrigin
public class UsersController {
	@Autowired
	UsersService usersService;
	
	@PostMapping(value="register",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String register(@RequestBody Users users) {
		return usersService.register(users);
	}
	@PostMapping(value="login",consumes=MediaType.APPLICATION_JSON_VALUE)
	public String loginUser(@RequestBody Users users) {
		return usersService.loginUser(users);
		}
	@GetMapping(value="logout")
	public String logout() {
		return"logout successfully";
	}
}
