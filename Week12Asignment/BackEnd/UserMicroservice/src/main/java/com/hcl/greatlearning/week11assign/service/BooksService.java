package com.hcl.greatlearning.week11assign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.week11assign.bean.Books;
import com.hcl.greatlearning.week11assign.dao.BooksDao;

@Service
public class BooksService {
  @Autowired
  BooksDao booksDao;
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}
	
}
