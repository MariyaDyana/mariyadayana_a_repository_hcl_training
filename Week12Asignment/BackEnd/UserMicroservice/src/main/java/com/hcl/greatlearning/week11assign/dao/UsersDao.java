package com.hcl.greatlearning.week11assign.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.week11assign.bean.Users;


@Repository
public interface UsersDao extends JpaRepository<Users,String> {

	public Users findByUsernameAndPassword(@Param("username") String username,@Param("password") String password);
	}
