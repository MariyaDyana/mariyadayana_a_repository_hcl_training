package com.hcl.greatlearning.week11assign.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Books {
	@Id
	private int id;
	private String name;
	private String generes;
	private String url;

	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Books(int id, String name, String generes, String url) {
		super();
		this.id = id;
		this.name = name;
		this.generes = generes;
		this.url = url;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGeneres() {
		return generes;
	}

	public void setGeneres(String generes) {
		this.generes = generes;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


}

