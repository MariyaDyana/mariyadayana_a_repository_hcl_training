import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../books';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-displaybook',
  templateUrl: './displaybook.component.html',
  styleUrls: ['./displaybook.component.css']
})
export class DisplaybookComponent implements OnInit {
  books:Array<Books>=[];
  constructor(public router:Router,public pser:UserserviceService) { }

  ngOnInit(): void {
  }
  loadBooks(): void{
    //console.log("Event fired")
    this.pser.loadBookDetails().subscribe(res=>this.books=res);
  }
}
