<jsp:include page="FooterPage.jsp" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.hcl.greatlearning.week7assignment.resource.DbResource"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html>
<%
Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<div>
<a href="ReadLater.jsp"><button class="btn"><b>Read Later</b></button></a>
<a href="LikedBook.jsp" ><button class="btn"><b>Liked</b></button></a>
<a href="logout.jsp" ><button class="btn"><b>Logout</b></button></a>

</div>
<h2 align="left"><font><strong>Books List</strong></font></h2>
<table align="left" cellpadding="5" cellspacing="5" border="1">
<tr>

</tr>
<tr>
<td><b>id</b></td>
<td><b>Books name</b></td>
<td><b>Read later</b></td>
<td><b>Like</b></td>

</tr>
<%
try{ 
	Connection con=DbResource.getDbConnection();
statement=con.createStatement();

resultSet = statement.executeQuery("SELECT * FROM books");
while(resultSet.next()){
%>
<tr>

<td><%=resultSet.getString("bookId") %></td>
<td><%=resultSet.getString("bookName") %></td>

<td><form action="LaterRead">
<input type = "hidden" name = "laterbookid" value = <%= resultSet.getString("bookId") %> />
<input type = "submit" value="Read Later">
</form></td>

<td><form action="LikedBook">
<input type = "hidden" name = "likedbookid" value = <%= resultSet.getString("bookId") %> />
<input type = "submit" value="Like">
</form></td>
</tr>

<% 
}

} catch (Exception e) {
e.printStackTrace();
}
%>
</table>
