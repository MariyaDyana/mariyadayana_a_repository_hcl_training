package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@SpringBootApplication(scanBasePackages = "com")
@EntityScan(basePackages = "com.hcl.greatlearning.miniproject.bean")
@EnableJpaRepositories(basePackages = "com.hcl.greatlearning.miniproject.dao")
public class MiniProjectWebFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectWebFrameworkApplication.class, args);
		System.out.println("This application is running in Port number 8080");
	}

}
