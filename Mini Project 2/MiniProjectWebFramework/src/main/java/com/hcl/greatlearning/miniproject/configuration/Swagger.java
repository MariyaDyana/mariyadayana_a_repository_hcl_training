package com.hcl.greatlearning.miniproject.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger {
	@Bean
	public Docket showbooksApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).groupName("SurabiRestaurant-API").select()
				.apis(RequestHandlerSelectors.basePackage("com.hcl.greatlearning.miniproject.controller")).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Surabi Restaurant APIs").description("Surabi Restaurant API Design ")
				.termsOfServiceUrl("http://fakesurabirestaurant.com")
				.contact((new Contact("SurabiRestaurant APIs", "http://surabirestaurant.com", "surabirestaurant@gmail.com")))
				.license("SurabiRestaurant License").licenseUrl("http://surabirestaurant.com").version("1.0").build();


	}

}
