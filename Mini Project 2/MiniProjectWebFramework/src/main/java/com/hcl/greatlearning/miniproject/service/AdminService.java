package com.hcl.greatlearning.miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.miniproject.bean.Admin;
import com.hcl.greatlearning.miniproject.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
	public void save(Admin admin) {
		adminDao.save(admin);
	}	
	
	
	public Admin adminLogin(String name, String password) {
		Admin admin = adminDao.findByNameAndPassword(name, password);
	  	return admin;
	  }
}
