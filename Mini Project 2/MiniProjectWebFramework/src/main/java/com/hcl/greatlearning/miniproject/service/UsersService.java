package com.hcl.greatlearning.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.miniproject.bean.Users;
import com.hcl.greatlearning.miniproject.dao.UsersDao;

@Service
public class UsersService {

	@Autowired
	UsersDao usersDao;

	

	public void save(Users user) {
		usersDao.save(user);
	}

	public Users login(String name, String password) {
		Users user = usersDao.findByNameAndPassword(name, password);
		return user;
	}

	public List<Users> showUser() {
		List<Users> user = usersDao.findAll();
		return user;
	}
}
