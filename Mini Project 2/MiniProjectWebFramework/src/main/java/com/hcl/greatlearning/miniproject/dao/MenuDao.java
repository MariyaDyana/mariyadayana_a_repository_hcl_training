package com.hcl.greatlearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.greatlearning.miniproject.bean.Menu;

public interface MenuDao extends JpaRepository<Menu, Integer> {

}
