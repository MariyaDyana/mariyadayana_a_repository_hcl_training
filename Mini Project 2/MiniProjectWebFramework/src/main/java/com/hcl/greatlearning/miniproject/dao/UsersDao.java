package com.hcl.greatlearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.greatlearning.miniproject.bean.Users;

public interface UsersDao extends JpaRepository<Users, Integer> {
	Users findByNameAndPassword(String name, String password);
}
