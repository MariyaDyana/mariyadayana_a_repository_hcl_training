package com.hcl.greatlearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.greatlearning.miniproject.bean.Admin;

public interface AdminDao extends JpaRepository<Admin, Integer>  {
	Admin findByNameAndPassword(String name, String password);
}
