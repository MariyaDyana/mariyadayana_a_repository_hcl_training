package com.hcl.greatlearning.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hcl.greatlearning.miniproject.bean.Users;
import com.hcl.greatlearning.miniproject.service.UsersService;

@Controller
public class RegisterController {

	@Autowired
	UsersService usersService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("user") Users user) {
		
		usersService.save(user);
		
		return "login";
	}
}
