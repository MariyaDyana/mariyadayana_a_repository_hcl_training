<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.List"%>
<%@ page import="com.hcl.greatlearning.miniproject.bean.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align=left>

		<%
		String name = (String) request.getAttribute("name");
		session.setAttribute("name", name);
		String username = (String) request.getAttribute("username");
		session.setAttribute("username", username);
		
		%>

		<%if(username!=null){ %>
		<h1 style="font-size: 15px"><%=username%>
			is removed...
		</h1>
		<% }%>
		
		<h1 style="font-size: 30px">List of Register Users</h1>

		<br>
		<table border="1" >
			<tr>
				<td>Sr. No</td>
				<td>User Name</td>
				<td>User Email Id</td>
				<td>Password</td>
			</tr>


			<%
			@SuppressWarnings("unchecked")
			List<Users> users = (List<Users>) request.getAttribute("list");
			%>

			<%
			int i = 1;
			for (Users user : users) {
			%>

			<tr>
				<td><%=i++%></td>
				<td><%=user.getName()%>
				<td><%=user.getEmail()%>
				<td><%=user.getPassword()%></td>
			</tr>
			<%
			}
			%>
		</table>
		<a href="logout" style="font-size: 30px"><button class="btn"><b>Logout</b></button> </a>
	</div>
</body>
</html>