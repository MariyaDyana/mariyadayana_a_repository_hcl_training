<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.List"%>
<%@ page import="com.hcl.greatlearning.miniproject.bean.Menu"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align=left>

		<%
		String name = (String) request.getAttribute("name");
		session.setAttribute("name", name);
		String menuName = (String) request.getAttribute("menuName");
		%>
		
		<%if(menuName!=null){ %>
		<h1 style="font-size: 15px"><%=menuName%>
			is deleted from Menu...
		</h1>
		<% }%>
		
		<h1 style="font-size: 75px">Welcome To Surabi Restaurants.....!</h1>
		
		<h1>Hello <%=name%></h1>

		<div align=left>

			
              <table border="1" style="background-color: #F0F8FF">

					<tr>
						<td>SR_NO</td>
						<td>Item_ID</td>
						<td>Item</td>
						<td>Total_Price</td>
						<td>Quantity</td>
						
					</tr>


					<%
					@SuppressWarnings("unchecked")
					List<Menu> menu = (List<Menu>) request.getAttribute("list");
					%>

					<%
					int i =1;
					for (Menu m : menu) {
					%>

					<tr>
						<td><%=i++%>
					     <td><%=m.getItemid()%></td>
					     <td><%=m.getItem()%></td>
					     <td><%=m.getTotal_price()%></td>
					     <td><%=m.getQuantity()%></td>
						
						
					</tr>
					<%
					}
					%>
			</table>
			<p>
			
			<br>
			<a href="showUsers?name=<%=name%>&amp" 
				style="font-size: 25px"><button class="btn"><b>Show Users</b></button></a>
			
			<a href="addMenu?name=<%=name%>&amp" 
				style="font-size: 25px"><button class="btn"><b>Add Menu</b></button></a> 
			
			<a href="logout" style="font-size: 25px"><button class="btn"><b>Logout</b></button></a>
		</p>
			
		</div>
	</div>
</body>
</html>