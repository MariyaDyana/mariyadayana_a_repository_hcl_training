<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<div align=left>

		<h1>Admin Register Account</h1>

		<form method="post" action="adminRegister">

			<p>User name</p>
			<input type="text" placeholder="name" name="name" required>

			<p>User email</p>
			<input type="text" placeholder="email" name="email" required>

			<p>Password</p>
			<input type="password" placeholder="password" name="password"
				required>

			<p>System Code</p>
			<input type="text" placeholder="systemCode" name="systemCode"
				required>

			<p>
				<input type="submit" value="adminRegister">
			</p>

			<p>
				<a href="adminLogin">click here to login</a>
			</p>

			<p>
				<a href="index"><button class="btn"><b>Home</b></button></a>
			</p>

		</form>
	</div>
</body>
</html>