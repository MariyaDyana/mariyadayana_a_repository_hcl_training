package com.miniproject.sr.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.miniproject.sr.pojo.AddtoFavourites;



public interface FavouriteRepository  extends CrudRepository<AddtoFavourites, Integer>{
	List<AddtoFavourites> findAll();
}
