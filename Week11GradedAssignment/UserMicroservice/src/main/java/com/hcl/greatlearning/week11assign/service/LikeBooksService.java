package com.hcl.greatlearning.week11assign.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.week11assign.bean.LikeBooks;
import com.hcl.greatlearning.week11assign.dao.LikeBooksDao;


@Service
public class LikeBooksService {
	@Autowired
	LikeBooksDao likeBooksDao;
	
	public List<LikeBooks> getAllLikeBooks() {
		return likeBooksDao.findAll();
	}
	
	public String storeLikeBooksInfo(LikeBooks likebook) {
				
						if(likeBooksDao.existsById(likebook.getId())) {
									return "likebook id must be unique";
						}else {
									likeBooksDao.save(likebook);
									return "likebook details stored successfully";
						}
	}
	
	public String deleteLikeBooksInfo(int id) {
		if(!likeBooksDao.existsById(id)) {
			return "likebook details not present";
			}else {
			likeBooksDao.deleteById(id);
			return "likebook details deleted successfully";
			}	
	}
	
	public String updateLikeBooksInfo(LikeBooks likebook) {
		if(!likeBooksDao.existsById(likebook.getId())) {
			return "likebook details not present";
			}else {
			LikeBooks p	= likeBooksDao.getById(likebook.getId());	// if product not present it will give exception 
			p.setName(likebook.getName());						// existing product price change 
			likeBooksDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "likebook details updated successfully";
			}	
	}
}
