package com.hcl.greatlearning.week11assign.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.week11assign.bean.LikeBooks;


@Repository
public interface LikeBooksDao extends JpaRepository<LikeBooks, Integer>  {

}
