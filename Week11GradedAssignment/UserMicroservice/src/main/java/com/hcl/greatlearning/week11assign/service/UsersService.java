package com.hcl.greatlearning.week11assign.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.week11assign.bean.Users;
import com.hcl.greatlearning.week11assign.dao.UsersDao;



@Service
public class UsersService {
	@Autowired
	   UsersDao usersDao;
	   
	   public String register(Users user) {
		   Users a=usersDao.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		   if(Objects.nonNull(a)) {
			   return "User name already exist";
		   }else {
			   usersDao.save(user);
			   return"User Details Registered Successfully";
		   }
		   
	   }
	   public String loginUser(Users user) {
		   Users ad=usersDao.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		   if(Objects.nonNull(ad)) {
			   return"Login Successfully";
		   }else {
			   return"Invalid Username or password";
		   }
	   }
}
