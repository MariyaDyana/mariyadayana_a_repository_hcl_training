package com.hcl.greatlearning.week11assign.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.hcl.greatlearning.week11assign.bean.Books;
import com.hcl.greatlearning.week11assign.dao.BooksDao;
import com.hcl.greatlearning.week11assign.service.BooksService;
@SpringBootTest
class BooksServiceTest {

	@InjectMocks
	BooksService booksService;
	
	@Mock
	BooksDao booksDao;
	@Test
	void testGetAllBooks() {
		//fail("Not yet implemented");
		List<Books> list =new ArrayList<Books>();
		Books book1=new Books();
		Books book2=new Books();
		Books book3=new Books();
		
		list.add(book1);
		list.add(book2);
		list.add(book3);
		when(booksDao.findAll()).thenReturn(list);
		List<Books> userslist=booksService.getAllBooks();
		assertEquals(3,userslist.size());
		verify(booksDao, times(1)).findAll();

	}

}
