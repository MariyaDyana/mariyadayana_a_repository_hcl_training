package com.hcl.greatlearning.week11assign.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.hcl.greatlearning.week11assign.bean.LikeBooks;
import com.hcl.greatlearning.week11assign.dao.LikeBooksDao;
import com.hcl.greatlearning.week11assign.service.LikeBooksService;

@SpringBootTest
class LikeBooksServiceTest {

	
	@InjectMocks
	LikeBooksService likebooksService;
	
	@Mock
	LikeBooksDao likebooksDao;
	@Test
	void testGetAllLikeBooks() {
		//fail("Not yet implemented");
		List<LikeBooks> list =new ArrayList<LikeBooks>();
		LikeBooks likebook1=new LikeBooks();
		LikeBooks likebook2=new LikeBooks();
		LikeBooks likebook3=new LikeBooks();
		
		list.add(likebook1);
		list.add(likebook2);
		list.add(likebook3);
		when(likebooksDao.findAll()).thenReturn(list);
		List<LikeBooks> userslist=likebooksService.getAllLikeBooks();
		assertEquals(3,userslist.size());
		verify(likebooksDao, times(1)).findAll();
	}

	@Test
	void testStoreLikeBooksInfo() {
		//fail("Not yet implemented");
		LikeBooks likebook=new LikeBooks();
		likebooksService.storeLikeBooksInfo(likebook);
		verify(likebooksDao, times(1)).save(likebook);
	}

	@Test
	void testDeleteLikeBooksInfo() {
		//fail("Not yet implemented");
		LikeBooks likebook=new LikeBooks();
		when(likebooksDao.findById(likebook.getId())).thenReturn(Optional.of(likebook));
		assertEquals("Deleted Successfully",likebooksService.deleteLikeBooksInfo(1));
		verify(likebooksDao,times(1)).delete(likebook);
	}

	@Test
	void testUpdateLikeBooksInfo() {
		fail("Not yet implemented");
	}

}
