package com.hcl.greatlearning.assign11.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.assign11.bean.LikeBooks;


@Repository
public interface LikeBooksDao extends JpaRepository<LikeBooks, Integer>  {

}

