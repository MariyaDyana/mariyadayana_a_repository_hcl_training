package com.hcl.greatlearning.assign11.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.greatlearning.assign11.bean.LikeBooks;
import com.hcl.greatlearning.assign11.service.LikeBooksService;

@RestController
@RequestMapping("/likebooks")
public class LikeBooksController {
	@Autowired
	LikeBooksService likeBooksService;
	
	@GetMapping(value = "getAllLikeBooks",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LikeBooks> getAllLikeBooksInfo() {
		return likeBooksService.getAllLikeBooks();
	}
	@PostMapping(value = "storeLikeBooks",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeLikeBooksInfo(@RequestBody LikeBooks likebook) {
		
				return likeBooksService.storeLikeBooksInfo(likebook);
	}
	
	@DeleteMapping(value = "deleteLikeBooks/{id}")
	public String storeLikeBooksInfo(@PathVariable("id") int id) {
					return likeBooksService.deleteLikeBooksInfo(id);
	}
	
	@PatchMapping(value = "updateLikeBooks")
	public String updateLikeBooksInfo(@RequestBody LikeBooks likebook) {
					return likeBooksService.updateLikeBooksInfo(likebook);
	}
}

