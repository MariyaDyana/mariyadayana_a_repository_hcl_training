package com.hcl.greatlearning.assign11.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.assign11.bean.Books;
import com.hcl.greatlearning.assign11.dao.BooksDao;



@Service
public class BooksService {

	@Autowired
	BooksDao booksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}
	
	public String storeBooksInfo(Books book) {
				
						if(booksDao.existsById(book.getId())) {
									return "book id must be unique";
						}else {
									booksDao.save(book);
									return "Product stored successfully";
						}
	}
	
	public String deleteBooksInfo(int id) {
		if(!booksDao.existsById(id)) {
			return "Books details not present";
			}else {
			booksDao.deleteById(id);
			return "Books deleted successfully";
			}	
	}
	
	public String updateBooksInfo(Books book) {
		if(!booksDao.existsById(book.getId())) {
			return "Product details not present";
			}else {
			Books p	= booksDao.getById(book.getId());	// if product not present it will give exception 
			p.setName(book.getName());						// existing product price change 
			booksDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "Product updated successfully";
			}	
	}
	}
