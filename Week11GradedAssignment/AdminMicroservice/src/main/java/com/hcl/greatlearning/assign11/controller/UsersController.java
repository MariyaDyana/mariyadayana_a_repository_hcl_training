package com.hcl.greatlearning.assign11.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.greatlearning.assign11.bean.Users;
import com.hcl.greatlearning.assign11.service.UsersService;


@RestController
@RequestMapping("/users")
public class UsersController {
	@Autowired
	UsersService usersService;
	
	@GetMapping(value = "getAllUsers",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Users> getAllUsersInfo() {
		return usersService.getAllUsers();
	}
	@PostMapping(value = "storeUsers",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUsersInfo(@RequestBody Users user) {
		
				return usersService.storeUsersInfo(user);
	}
	
	@DeleteMapping(value = "deleteUsers/{uid}")
	public String storeUsersInfo(@PathVariable("uid") int uid) {
					return usersService.deleteUsersInfo(uid);
	}
	
	@PatchMapping(value = "updateUsers")
	public String updateUsersInfo(@RequestBody Users users) {
					return usersService.updateUsersInfo(users);
	}
}

