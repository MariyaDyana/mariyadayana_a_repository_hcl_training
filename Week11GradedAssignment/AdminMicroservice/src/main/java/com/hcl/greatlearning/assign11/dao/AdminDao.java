package com.hcl.greatlearning.assign11.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.assign11.bean.Admin;



@Repository
public interface AdminDao extends JpaRepository<Admin,String> {
    
	public Admin findByAdminnameAndPassword(@Param("adminname") String adminname,@Param("password") String password);
}
