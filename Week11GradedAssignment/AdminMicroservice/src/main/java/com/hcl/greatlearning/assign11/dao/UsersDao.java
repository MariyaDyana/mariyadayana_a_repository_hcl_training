package com.hcl.greatlearning.assign11.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.greatlearning.assign11.bean.Users;
@Repository
public interface UsersDao extends JpaRepository<Users, Integer> {

}

