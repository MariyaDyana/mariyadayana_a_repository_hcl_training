package com.hcl.greatlearning.assign11.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import com.hcl.greatlearning.assign11.bean.Users;
import com.hcl.greatlearning.assign11.dao.UsersDao;
import com.hcl.greatlearning.assign11.service.UsersService;
@SpringBootTest
class UsersServiceTest {
	
	@InjectMocks
	UsersService usersService;
	
	@Mock
	UsersDao usersDao;
	

	//@Test
	void testGetAllUsers() {
		//fail("Not yet implemented");
		List<Users> list =new ArrayList<Users>();
		Users user1=new Users();
		Users user2=new Users();
		Users user3=new Users();
		list.add(user1);
		list.add(user2);
		list.add(user3);
		when(usersDao.findAll()).thenReturn(list);
		List<Users> userslist=usersService.getAllUsers();
		assertEquals(3,userslist.size());
		verify(usersDao, times(1)).findAll();
		
		
		
	}
	


	//@Test
	void testStoreUsersInfo() {
		//fail("Not yet implemented");
		Users user=new Users();
		usersService.storeUsersInfo(user);
		verify(usersDao, times(1)).save(user);
	}

	@Test
	void testDeleteUsersInfo() {
		//fail("Not yet implemented");
		Users user=new Users();
		when(usersDao.findById(user.getUid())).thenReturn(Optional.of(user));
		assertEquals("Deleted Successfully",usersService.deleteUsersInfo(1));
		verify(usersDao,times(1)).delete(user);
	}

	//@Test
	void testUpdateUsersInfo() {
		fail("Not yet implemented");
	}

}
